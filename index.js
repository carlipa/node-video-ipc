// sudo -u carlipa PID_WATCH=$(pgrep Xorg) MPV_BIN=/usr/bin/mpv node .

const spawn = require('child_process').spawn;
const path = require('path');
const IPC = require('easy-ipc');
const usage = require('usage');
const _ = require('lodash');
const prettyBytes = require('pretty-bytes');
const string = require('string');
const ps = require('ps-node');
const fs = require('fs');

let connection = null;
let playIndex = 0;

const mpvBinByPlatform = {
  linux: '/usr/bin/mpv',
  darwin: '/usr/local/bin/mpv'
};

const defaultConfig = {
  paths: {
    ipcSocket: path.resolve('/tmp/video-ipc-mpv-%.socket'),
    mediaDir:  path.resolve(__dirname, 'files'),
    mpv: process.env.MPV_BIN || mpvBinByPlatform[process.platform],
  },
  args: [
    '--quiet'
  ],
  media: [
    '01.mp4'
  ],
  ipcTimer: 250
}

let sequence = 1;
if (process.argv.length > 2) {
  sequence = parseInt(process.argv[2], 10);
}

const mpvInstances = [];
const ipcConnections = [];

fs.readFile(path.resolve(__dirname, 'sequences', `${sequence}.json`), (err, buf) => {
  if (err) throw new Error(`Unknown sequence ${sequence}`);
  const sequenceConfig = JSON.parse(buf.toString());
  const config = _.defaults({}, sequenceConfig, defaultConfig);

  config.windows.forEach((winConf, index) => {
    let args = config.args.concat([`--input-ipc-server=${config.paths.ipcSocket.replace('%', index)}`]);
    if (winConf.fullscreen) {
      args.push('--fullscreen');
    } else if (winConf.geometry) {
      args.push(`--geometry=${winConf.geometry}`);
    }
    if (winConf.args) {
      args = args.concat(winConf.args);
    }
    mpvInstances[index] = spawn(config.paths.mpv, args, {cwd: __dirname, env: {DISPLAY: ':0'}});
    mpvInstances[index].stdout.on('data', (data) => console.log(data.toString().replace(/\n$/, '')));
    mpvInstances[index].stderr.on('data', (data) => console.error(data.toString().replace(/\n$/, '')));
    mpvInstances[index].on('exit', (code) => console.log(`Exiting instance ${index} with code ${code}`));

    setTimeout(() => {
      const ipc = new IPC({
        socketPath: config.paths.ipcSocket.replace('%', index)
      });

      ipc.on('connect', (conn) => {
        ipcConnections[index] = conn;
        startPlay(index, config);
      })
      .on('error', (err) => console.error('IPC', err))
      .on('close', () => console.log('ipc closed'));

      ipc.connect();
    }, config.ipcTimer);
  });
});

let countPlay = 1;
function startPlay(index, config) {
  console.log(countPlay++);
  ipcConnections[index].write({command: ['loadfile', path.resolve(config.paths.mediaDir, config.media[playIndex]), 'replace']});
  playIndex = (playIndex + 1) % config.media.length;

  let timer = 1000;
  if (config.playTimer) {
    timer = config.playTimer;
  } else if (config.playTimerMin && config.playTimerMax) {
    timer = _.random(config.playTimerMin, config.playTimerMax);
  }

  setTimeout(() => {
    startPlay(index, config);
  }, timer);
}

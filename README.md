# node-video-ipc

## quickstart

1. Install nvm and nodejs

```bash
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash
. ~/.nvm/nvm.sh
nvm install --lts
```

1. Clone project (26MB due to bundled videos) and install deps


```bash
git clone git@gitlab.com:carlipa/node-video-ipc.git
cd node-video-ipc
npm i
```

1. Run test sequence

```bash
npm start 1 # or other sequence number 2, 3, 4...
```

1. Check any leaks

```bash
watch -n1 'free -mh'
```


## Sequences

1. Fullscreen single video test
2. 16 videos (480x270) with random change
3. 9 videos (640x360) with random change
3. 4 videos (960x540) with random change
